package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DecisionMaker {

    private ContentReader contentReader;

    public DecisionMaker() {
        this.contentReader = new ContentReader();
    }

    /**
     * <p>
     * Parse line and try to find all possible best solutions comparing the weight and price.
     * (there may be more than one best solutions when the weight and price are all equal)</p>
     * <p>
     * When there is no solution, will return empty list; on the output side will be an empty line.
     * </p>
     * This method is not intended to be overriden.
     * @param line line of data
     * @return all best solutions
     * @throws APIException when some data does not meed constraint, like weight > 100 or elements more
     * then 15
     */
    List<List<Item>> lineSolution(String line) throws APIException {
        Integer posColon = line.indexOf(':');
        String weightStr = line.substring(0, posColon).trim();
        try {
            Integer weight = Integer.parseInt(weightStr);
            if (weight > 100) throw new APIException("Max weight should be less than or equal to 100");
            line = line.substring(posColon+1).trim();
            List<Item> items = contentReader.parseLine(line);
            if (items.size() > 15) throw new APIException("Cannot receive more than 15 items");
            Collections.sort(items);
            Collections.reverse(items);
            Integer maxPrice = 0;
            BigDecimal minWeight = BigDecimal.ZERO;
            List<Item> finalDecision = null;
            List<List<Item>> solutions = new ArrayList<>();
            for (int i=0; i<items.size(); i++) {
                BigDecimal weightLimit = new BigDecimal(weightStr);
                Integer totalPrice = 0;
                BigDecimal totalWeight = BigDecimal.ZERO;
                List<Item> solution = new ArrayList<>();
                if (weightLimit.subtract(items.get(i).getWeight()).compareTo(BigDecimal.ZERO) >= 0) {
                    weightLimit = weightLimit.subtract(items.get(i).getWeight());
                    solution.add(items.get(i));
                    totalWeight = totalWeight.add(items.get(i).getWeight());
                    totalPrice += items.get(i).getPrice();
                    for (int j=i+1; j<items.size(); j++) {
                        if (weightLimit.subtract(items.get(j).getWeight()).compareTo(BigDecimal.ZERO) >= 0) {
                            weightLimit = weightLimit.subtract(items.get(j).getWeight());
                            totalWeight = totalWeight.add(items.get(j).getWeight());
                            totalPrice += items.get(j).getPrice();
                            solution.add(items.get(j));
                        }
                        break;
                    }
                }

                if (!solution.isEmpty()) {
                    if (totalPrice > maxPrice) {
                        finalDecision = solution;
                        minWeight = totalWeight;
                        maxPrice = totalPrice;
                    } else if (totalPrice == maxPrice) {
                        if (minWeight.compareTo(totalWeight) > 0) {
                            finalDecision = solution;
                        } else if (minWeight.compareTo(totalWeight) == 0) { // also best option
                            solutions.add(solution); // second best solution
                        }
                    }

                }


            }
            if (finalDecision != null) { // when there is no solution
                solutions.add(finalDecision);
            }
            return solutions;
        } catch (Exception e) {
            throw optionalWrap("Cannot pack. ", e);
        }
    }

    /**
     * Optionally wrap exception(only wrap the original one; if is already {@link APIException}
     * will throw as-is
     * @param reason
     * @param e
     * @return
     */
    APIException optionalWrap(String reason, Exception e) {
        if (e instanceof APIException) {
            return (APIException) e;
        } else {
            return new APIException(reason, e);
        }
    }


}
