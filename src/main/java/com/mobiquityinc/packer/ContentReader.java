package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class ContentReader {

    private DataValidator dataValidator;

    public ContentReader() {
        this.dataValidator = new DataValidator();
    }
    /**
     * Read file from absolute path and parse content to form lines
     * @param absolutePath absolute path of file
     * @return content in lines
     * @throws com.mobiquityinc.exception.APIException if file does not exist,
     * and/or other IO error
     */
    List<String> readFileAndParse(String absolutePath) throws APIException {
        File file = new File(absolutePath);
        if (!file.exists()) throw new APIException("File does not exist: " + absolutePath);
        try (FileInputStream fileInputStream = new FileInputStream(file);
             Reader reader = new InputStreamReader(new BufferedInputStream(fileInputStream),
                     Charset.forName("UTF-8"));
             BufferedReader bufferedReader = new BufferedReader(reader)) {
            List<String> lines = new ArrayList<>();
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            return lines;
        } catch (Exception e) { // SecurityException or IOException
            throw new APIException("Cannot read file with path " + absolutePath, e);
        }
    }

    /**
     * Form list of {@link Item} with data in the line
     * @param line the line without the weight limit and the following " : "
     * @return list of {@link Item}, or empty when the line is empty
     */
    List<Item> parseLine(String line) throws APIException {
        String[] objectData = line.trim().split(" ");
        List<Item> items = new ArrayList<>(objectData.length);
        for (String object: objectData) {
            String[] properties = object.replace("(", "").replace(")", "")
                .split(",");
            try {
                Item newItem = new Item(
                        Integer.parseInt(properties[0]),
                        properties[1],                                  // precise construct
                        Integer.parseInt(properties[2].substring(1))    // get rid of euro sign
                );
                if (!dataValidator.weightInRange(newItem.getWeight())) {

                }
                if (newItem.getPrice() > 100) {
                    throw new IllegalArgumentException("Item price cannot surpass 100");
                }
                items.add(newItem);
            } catch (Exception e) {
                throw new APIException("Cannot parse data in the line; data: " + Arrays.toString(properties), e);
            }
        }
        return items;
    }
}
