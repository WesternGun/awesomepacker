package com.mobiquityinc.packer;

import java.math.BigDecimal;
import java.util.Objects;

public class Item implements Comparable<Item> {
    private final Integer index;
    private final BigDecimal weight;
    private final Integer price;

    public Integer getIndex() {
        return index;
    }


    public BigDecimal getWeight() {
        return weight;
    }


    public Integer getPrice() {
        return price;
    }

    public Item(Integer index, String weight, Integer price) {
        this.index = index;
        this.weight = new BigDecimal(weight);
        this.price = price;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return getIndex().equals(item.getIndex()) &&
                getWeight().compareTo(item.getWeight()) == 0 && // compareTo() of BigDecimal ignores scale
                getPrice().equals(item.getPrice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIndex(), getWeight(), getPrice());
    }

    @Override
    public int compareTo(Item o) {
        if (this.getPrice().compareTo(o.price) == 0) {
            return this.getWeight().compareTo(o.getWeight());
        } else {
            return this.getPrice().compareTo(o.getPrice());
        }

    }
}
