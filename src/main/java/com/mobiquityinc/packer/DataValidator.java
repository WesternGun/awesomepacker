package com.mobiquityinc.packer;

import java.math.BigDecimal;

public class DataValidator {
    private static final Integer ITEM_NUMBER_LIMIT = 15;
    private static final BigDecimal WEIGHT_LIMIT = BigDecimal.valueOf(100);

    public boolean itemsNumberInRange(Integer itemsNumber) {
        return itemsNumber <= ITEM_NUMBER_LIMIT;
    }

    public boolean weightInRange(BigDecimal weight) {
        return weight.compareTo(WEIGHT_LIMIT) <= 0;
    }
}
