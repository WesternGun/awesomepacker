package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;

import java.util.ArrayList;
import java.util.List;

public class Packer {

    private static DecisionMaker decisionMaker;
    private static ContentReader contentReader;

    private Packer() {
         decisionMaker = new DecisionMaker();
         contentReader = new ContentReader();
    }

    /**
     * <p>
     *     Choose solution of packing and print as string in lines. Each line is a best solution
     *      * to a line of data.
     * </p>
     * <p>
     *     One line in the input file may correspond to severals in the output and no separation is
     *     added(because there may be more than one best solutions of packaging). TODO
     * </p>
     *
     * @param filePath Absolute path of file of data
     * @return output indicating all best solutions of packaging
     * @throws APIException when data does not meet constraints, or when any I/O errors happens
     */
    public static String pack(String filePath) throws APIException {
        List<String> fileContent = contentReader.readFileAndParse(filePath);
        List<String> output = new ArrayList<>();
        for (String line : fileContent) {
            List<List<Item>> solutions = decisionMaker.lineSolution(line);
            List<String> allLines = new ArrayList<>();
            for (List<Item> solution: solutions) {
                List<String> bestIndexes = new ArrayList<>();
                for (Item item: solution) {
                    bestIndexes.add(item.getIndex().toString());
                }
                String lineIndexes = String.join(",", bestIndexes);
                allLines.add(lineIndexes);
            }
            output.add(String.join(System.lineSeparator(), allLines));
        }
        return String.join(System.lineSeparator(), output);
    }
}
