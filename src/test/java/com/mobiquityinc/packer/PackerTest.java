package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PackerTest {

    @ParameterizedTest(name = "{0}")
    @MethodSource("bestSolutionInput")
    public void shouldReturnCorrectOutput(String testName, String fileName, String output) throws APIException {
        // given
        String filePath = mapFixtureToFile(fileName);
        // when
        String result = Packer.pack(filePath);

        // then
        assertEquals(output, result);
    }

    private static Stream<Arguments> bestSolutionInput() {
        return Stream.of(
                Arguments.of("should return output", "fixture1.txt",
                        String.join(System.lineSeparator(), new String[]{
                                "1", "1", "2,3", "1", "2,3", "2,3", ""
                        }))
        );
    }

    private String mapFixtureToFile(String fileName) {
        return Paths.get("./src/test/resources/" + fileName).normalize().toAbsolutePath().toString();
    }
}