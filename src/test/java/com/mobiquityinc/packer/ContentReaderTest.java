package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ContentReaderTest {
    private ContentReader contentReader = new ContentReader();

    @Test
    public void shouldFindExampleFileAndReadAllLines() throws APIException {
        // given
        Path root = Paths.get("./src/test/resources/example.txt").normalize().toAbsolutePath();
        System.out.println("Example file path: " + root);
        assertEquals(true, root.toFile().exists());
        String firstLine = "81 : (1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        String secondLine = "8 : (1,15.3,€34)";
        String thirdLine = "75 : (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74) (8,93.18,€35) (9,89.95,€78)";
        String fourthLine = "56 : (1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64)";

        // when
        List<String> content = contentReader.readFileAndParse(root.toString());

        // then
        assertEquals(4, content.size());
        assertEquals(firstLine, content.get(0));
        assertEquals(secondLine, content.get(1));
        assertEquals(thirdLine, content.get(2));
        assertEquals(fourthLine, content.get(3));
    }

    @Test
    public void parseLine() throws APIException {
        // given
        String line = "(1,53.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)";
        Item item11 = new Item(1, "53.38", 45);
        Item item12 = new Item(2, "88.62", 98);
        Item item13 = new Item(3, "78.48", 3);
        Item item14 = new Item(4, "72.30", 76);
        Item item15 = new Item(5, "30.18", 9);
        Item item16 = new Item(6, "46.34", 48);
        // when
        List<Item> items = contentReader.parseLine(line);

        assertEquals(6, items.size());
        assertEquals(item11, items.get(0));
        assertEquals(item12, items.get(1));
        assertEquals(item13, items.get(2));
        assertEquals(item14, items.get(3));
        assertEquals(item15, items.get(4));
        assertEquals(item16, items.get(5));

    }
}