package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DecisionMakerTest {

    private static Item item1;
    private static Item item2;
    private static Item item3Expensive;
    private static Item item3Heavier;
    private static Item item3EqualWeight;
    private static Item item3Lighter;
    private static List<Item> onlyItem1;
    private static List<List<Item>> group1;
    private static List<Item> item2AndItem3Heavier;
    private static List<List<Item>> group2;
    private static List<Item> item2AndItem3EqualWeight;
    private static List<List<Item>> group3;
    private static List<Item> item2AndItem3Lighter;
    private static List<List<Item>> group4;
    private static List<Item> item2AndItem3Best;
    private static List<List<Item>> group5;

    @BeforeAll
    private static void setup() {
        item1 = new Item(1, "72.3", 76);
        item2 = new Item(2, "46.3", 48);
        // with equal value but heavier, discard
        item3Heavier = new Item(3, "30", 28);
        // with equal value and equal weight, also choose
        item3EqualWeight = new Item(3, "26", 28);
        // with equal value and lighter, choose
        item3Lighter = new Item(3, "24", 28);
        // with less weight but more value, definitely choose
        item3Expensive = new Item(3, "30", 33);

        // group 1
        onlyItem1 = Collections.singletonList(item1);
        group1 = Collections.singletonList(onlyItem1);

        // group 2
        item2AndItem3Heavier = new ArrayList<>();
        item2AndItem3Heavier.add(item2);
        item2AndItem3Heavier.add(item3Heavier);
        group2 = new ArrayList<>();
        group2.add(onlyItem1);
        group2.add(item2AndItem3Heavier);

        // group 3
        item2AndItem3EqualWeight = new ArrayList<>();
        item2AndItem3EqualWeight.add(item2);
        item2AndItem3EqualWeight.add(item3EqualWeight);
        group3 = new ArrayList<>();
        group3.add(onlyItem1);
        group3.add(item2AndItem3EqualWeight);

        // group 4
        item2AndItem3Lighter = new ArrayList<>();
        item2AndItem3Lighter.add(item2);
        item2AndItem3Lighter.add(item3Lighter);
        group4 = new ArrayList<>();
        group4.add(onlyItem1);
        group4.add(item2AndItem3Lighter);

        // group 5
        item2AndItem3Best = new ArrayList<>();
        item2AndItem3Best.add(item2);
        item2AndItem3Best.add(item3Expensive);
        group5 = new ArrayList<>();
        group5.add(onlyItem1);
        group5.add(item2AndItem3Best);
    }


    @ParameterizedTest(name = "{0}")
    @MethodSource("bestSolutionCorrectInput")
    public void shouldReturnCorrectSolutions(String testName, String input, List<List<Item>> expected) throws APIException {
        // given
        DecisionMaker decisionMaker = new DecisionMaker();
        // when
        List<List<Item>> result = decisionMaker.lineSolution(input);

        // then
        assertTrue(result.containsAll(expected));
    }

    private static Stream<Arguments> bestSolutionCorrectInput() {
        String line1 = "81 : (1,72.3,€76)";
        String line2 = "81 : (1,72.3,€76) (2,46.3,€48) (3,30,€28)";
        String line3 = "81 : (1,72.3,€76) (2,46.3,€48) (3,26,€28)";
        String line4 = "81 : (1,72.3,€76) (2,46.3,€48) (3,24,€28)";
        String line5 = "81 : (1,72.3,€76) (2,46.3,€48) (3,30,€33)";
        String line6 = "81 : (1,90,€76)";
        return Stream.of(
                Arguments.of("should find solution(the only element)", line1, group1),
                Arguments.of("should find best solution: first", line2, group1),
                Arguments.of("should find two solutions equal in weight and value", line3, group3),
                Arguments.of("should find the best solution: the latter one which is lighter", line4,
                        Collections.singletonList(item2AndItem3Lighter)),
                Arguments.of("should find best solution: the second is heavier but more expensive", line5, Collections.singletonList(item2AndItem3Best)),
                Arguments.of("should not pick any because there is no solution", line6, new ArrayList<>())
        );
    }

    @ParameterizedTest(name = "{0}")
    @MethodSource("exceptionInput")
    public void shouldWrapCorrectly(String testName, Exception original, String reason, Exception expected) {
        // given
        DecisionMaker decisionMaker = new DecisionMaker();
        Exception result = decisionMaker.optionalWrap(reason, original);

        assertEquals(expected.getCause(), result.getCause());
        assertEquals(expected.getClass(), result.getClass());
    }

    private static Stream<Arguments> exceptionInput() {
        Exception inner = new IllegalArgumentException("wrong!");
        APIException wrapped = new APIException("something", inner);
        APIException unwrapped = new APIException("anything");
        return Stream.of(
                Arguments.of("should wrap a primitive exception", inner, "something", wrapped),
                Arguments.of("should not wrap APIException", unwrapped, "anything", unwrapped)
        );
    }
}
